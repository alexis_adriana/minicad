package objetos;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JSlider;

import eventos.EventosObjetos;

public class Slider extends JSlider {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	EventosObjetos objeto_seleccionado;
	JSlider yo;
	boolean run = false;
	
	public Slider()
	{
		super(JSlider.HORIZONTAL, 0, 100, 50);
		this.setInverted(false); //se invierte el relleno del JSlider (desde donde comienza)
		this.setPaintTicks(true);
		this.setMajorTickSpacing(25);
		this.setMinorTickSpacing(5);
		this.setPaintLabels(false);
		this.yo = this;
		this.addMouseListener(new MouseListener() {
			
			int ancho, alto;
			
			public void mouseReleased(MouseEvent e) {
				//System.out.println(yo.getValue());
				//if(run) {
					int porcentaje = yo.getValue();
					System.out.println("Porcentaje: " + porcentaje + " " + yo.getMaximum());
					//if (porcentaje < yo.getMaximum()) {
					int nancho = (ancho * porcentaje) / 100;
					int nalto = (alto * porcentaje) / 100;
					System.out.println("Tamano: " + nancho + " " + nalto);
					objeto_seleccionado.Tamano(nancho, nalto);
					objeto_seleccionado.setEscala(porcentaje);
					objeto_seleccionado.Recalcular();
					//}
					run = false;
				/*} else {
					run = true;
				}*/
			}

			@Override
			public void mouseClicked(MouseEvent arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseEntered(MouseEvent arg0) {
				// TODO Auto-generated method stub
				
				if (objeto_seleccionado != null)
				{
					ancho = objeto_seleccionado.getAnchoInicial();
					alto = objeto_seleccionado.getAltoInicial();
				}
			}

			@Override
			public void mouseExited(MouseEvent arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mousePressed(MouseEvent arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		//this.addChangeListener(new ChangeListener() {
		//	public void stateChanged(ChangeEvent e) {
		//	}
		//});
	}
	
	public void setObjeto(EventosObjetos objeto_seleccionado)
	{
		this.objeto_seleccionado = objeto_seleccionado;
	}
}
