package ventana;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Color;
import java.awt.Font;

import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import objetos.Panel;
import objetos.ColorChooserButton;
import objetos.Slider;
import objetos.BotonRotar;

public class Ventana extends JFrame {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	String objeto;
	Color color_seleccionado = Color.red;
	Panel panel_fondo;
	
	public Ventana()
	{
		super("MiniCAD");
		JPanel panel_principal = new JPanel();
		panel_principal.setLayout(null);
		panel_principal.setBounds(0, 0, 800, 32);
                panel_principal.setLocation(100,100);
                panel_principal.setBackground(Color.pink);
                
                JLabel titulo = new JLabel("-MiniCAD-");
                
                titulo.setBounds(300, 10, 220, 50);
                titulo.setForeground(Color.GRAY);
                titulo.setFont(new Font("Tahoma", Font.BOLD, 40));
                          
                
              	Slider slider = new Slider();
		slider.setBounds(620, 0, 150,70);
                slider.setForeground(Color.GRAY);
                slider.setOpaque(false);
		
		BotonRotar boton_izquierda = new BotonRotar("+");
		boton_izquierda.setBounds(20, 10, 50, 50);
		BotonRotar boton_derecha = new BotonRotar("-");
		boton_derecha.setBounds(90, 10, 50, 50);
		
		this.panel_fondo = new Panel(this);
		this.panel_fondo.setBounds(0, 74, 700, 568);
		panel_fondo.setSlider(slider);
		panel_fondo.setRotador(boton_izquierda, "+");
		panel_fondo.setRotador(boton_derecha, "-");
                //panel_fondo.setBackground(Color.PINK);
                panel_fondo.setOpaque(true);
		panel_principal.add(this.panel_fondo);
		
		JButton boton_rectangulo = new JButton();
		boton_rectangulo.setIcon(new ImageIcon(ClassLoader.getSystemResource("varios/rectangulo_1.png")));
		boton_rectangulo.setBounds(716, 72 , 50, 50);
		boton_rectangulo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				objeto = "Rectangulo";
				panel_fondo.setObjeto(objeto);
				
			}
		});
		
		JButton boton_linea = new JButton();
		boton_linea.setIcon(new ImageIcon(ClassLoader.getSystemResource("varios/Palito.png")));
		boton_linea.setBounds(716, 135, 50, 50);
		boton_linea.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				objeto = "Linea";
				panel_fondo.setObjeto(objeto);
				
			}
		});
		
		JButton boton_circulo = new JButton();
		boton_circulo.setIcon(new ImageIcon(ClassLoader.getSystemResource("varios/Circulo.png")));
		boton_circulo.setBounds(716, 198, 50, 50);
		boton_circulo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				objeto = "Circulo";
				panel_fondo.setObjeto(objeto);
				
			}
		});
		
		JButton boton_triangulo = new JButton();
		boton_triangulo.setIcon(new ImageIcon(ClassLoader.getSystemResource("varios/Triangulo.png")));
		boton_triangulo.setBounds(716, 261, 50, 50);
		boton_triangulo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				objeto = "Triangulo";
				panel_fondo.setObjeto(objeto);
				
			}
		});
		
		JButton boton_libre = new JButton();
		boton_libre.setIcon(new ImageIcon(ClassLoader.getSystemResource("varios/Dedo.png")));
		boton_libre.setBounds(716, 324, 50, 50);
		boton_libre.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				objeto = "Libre";
				panel_fondo.setObjeto(objeto);
				
			}
		});
	        
                
                
		ColorChooserButton ccb = new ColorChooserButton(Color.pink);
		ccb.setBounds(716, 387, 50, 50);
		ccb.addColorChangedListener(new ColorChooserButton.ColorChangedListener() {
		    @Override
		    public void colorChanged(Color newColor) {
		    	color_seleccionado = newColor;
		    	panel_fondo.setColor(newColor);
		    	
		    	//System.out.println("Desde Ventana: " + color_seleccionado.toString());
		            
		    }
		});
		
		//JLabel lblcolor = new JLabel("Cambiar Color");
		//lblcolor.setBounds(192, 0, 150, 32);
		
		panel_principal.add(boton_rectangulo);
		panel_principal.add(boton_linea);
		panel_principal.add(boton_circulo);
		panel_principal.add(boton_triangulo);
		panel_principal.add(boton_libre);
		panel_principal.add(ccb);
		//panel_principal.add(lblcolor);
		panel_principal.add(slider);
		panel_principal.add(boton_izquierda);
		panel_principal.add(boton_derecha);
		panel_principal.add(titulo);
		
                
                this.add(panel_principal);
		this.setSize(800, 600);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setVisible(true);
	}
}
