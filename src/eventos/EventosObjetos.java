package eventos;

import java.awt.Color;
import java.awt.Graphics;

import objetos.BotonRotar;
import objetos.ContenedorFigura;
import objetos.Slider;

public interface EventosObjetos 
{
	
	public void Mover(int x, int y);
	public void Tamano(int width, int height);
	public void Calcular();
	public void Recalcular();
	public int getX();
	public int getY();
	public int getWidth();
	public int getHeight();
	public void dibujar();
	public void setColor(Color color);
	public void setSlider(Slider slider);
	public void setEscala(int escala);
	public Graphics getGraficos();
	public int getAngulo();
	public void setAngulo(int angulo);
	public void setRotador(BotonRotar rotador, String accion);
	public void setPanel(ContenedorFigura panel);
	public ContenedorFigura getPanel();
	public void Actualizar(Graphics g);
	public int getAnchoInicial();
	public int getAltoInicial();
	//public void addPunto(Point2D punto);
	//public void addPunto(float x, float y);
	
}
